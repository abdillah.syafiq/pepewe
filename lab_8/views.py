from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

def index(request):
    response = {'author': "Syafiq Abdillah"}
    html = 'lab_8/lab_8.html'
    return render(request, html, response)
